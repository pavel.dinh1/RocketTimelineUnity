﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
 public float movementSpeed = 10f;
 public static bool isInputDisabled = false;

 // Update is called once per frame
 void Update()
 {
  if (isInputDisabled == false)
  {
   float X = Input.GetAxis("Horizontal");
   float Y = Input.GetAxis("Vertical");

   transform.position += new Vector3(X * movementSpeed * Time.deltaTime, 0, Y * movementSpeed * Time.deltaTime);
  }
 }
}
