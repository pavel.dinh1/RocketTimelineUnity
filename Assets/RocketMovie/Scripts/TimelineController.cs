﻿using UnityEngine;
using UnityEngine.Playables;
using System.Collections.Generic;

// Pretahnout na objekt s PlayableDirector komponentou
public class TimelineController : MonoBehaviour
{
 public List<PlayableDirector> playableDirectors;
 public bool spustitPriStartu = false;

 // Spusteni pri startu
 void Start()
 {
  // Kontroluju jestli mam pretazeni objekt 'Timeline' do promenne

  foreach (PlayableDirector pd in playableDirectors)
  {
   if (pd != null && spustitPriStartu)
   {
    pd.playOnAwake = true;
   }
  }
 }
}
