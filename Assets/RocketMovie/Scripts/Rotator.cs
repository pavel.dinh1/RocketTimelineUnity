﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
 public float speed = 10f;
 public GameObject customCamera = null;
 public bool isActive = false;
 private void Start()
 {
  if (isActive)
   this.gameObject.SetActive(false);
 }

 private IEnumerator Rotate(float tick)
 {
  if (customCamera != null)
  {
   customCamera.transform.Rotate(0f, speed, 0f);
  }
  else
  {
   transform.Rotate(0f, speed, 0f);
  }
  yield return new WaitForSeconds(tick);
  yield return StartCoroutine(Rotate(tick));
 }
 private void OnEnable()
 {
  StartCoroutine(Rotate(0f));
 }

 private void StopCoroutine()
 {
  StopCoroutine(Rotate(0));
 }
}
